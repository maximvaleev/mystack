﻿namespace Stack
{
    public static class MyStackExtensions
    {
        // st2 будет опустошен
        public static void Merge<T>(this MyStack<T> st1, ref MyStack<T> st2)
        {
            bool isSuccess;
            do
            {
                isSuccess = st2.TryPop(out T? value);
                if (isSuccess)
                {
                    st1.Push(value);
                }
            } while (isSuccess);
        }
    }
}
