﻿namespace Stack
{
    public partial class MyStack<T>
    {
        // Реализация допускает помещение в стек элементов со значением null
        // В качестве ориентира по поведению класса и именованю методов и свойств брал System.Collections.Generic.Stack<T>
        // создает много мусора

        public int Count { get; private set; }
        private StackItem<T>? TopItem { get; set; }

        public MyStack(params T?[] values)
        {
            TopItem = null;
            Count = 0;

            foreach (T? value in values)
            {
                Push(value);
            }
        }

        public void Push(T? value)
        {
            TopItem = new StackItem<T>(value, TopItem);
            Count++;
        }

        public T? Peek()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException("Стэк пуст.");
            }
            return TopItem!.Value;
        }

        public T? Pop()
        {
            T? valueToReturn = Peek();
            TopItem = TopItem!.PreviousItem;
            Count--;
            return valueToReturn;
        }

        public bool TryPeek(out T? result)
        {
            if (Count > 0) 
            { 
                result = Peek();
                return true;
            }
            result = default;
            return false;
        }

        public bool TryPop(out T? result)
        {
            if (Count > 0)
            {
                result = Pop();
                return true;
            }
            result = default;
            return false;
        }

        public void Clear()
        {
            Count = 0;
            TopItem = null;
        }

        private MyStack<T> DeepCopy()
        {
            T?[] values = new T[Count];
            StackItem<T>? currentItem = TopItem;
            for (int i = Count - 1; i >= 0; i--)
            {
                values[i] = currentItem!.Value;
                currentItem = currentItem.PreviousItem;
            }
            return new MyStack<T>(values);
        }

        public static MyStack<T> Concat(params MyStack<T>[] stacks)
        {
            // скопировать исходные стеки, чтобы не повлиять на них при работе
            for (int i = 0; i < stacks.Length; i++)
            {
                stacks[i] = stacks[i].DeepCopy();
            }
            
            MyStack<T> result = new();
            for (int i = 0; i < stacks.Length; i++)
            {
                bool isSuccess;
                do
                {
                    isSuccess = stacks[i].TryPop(out T? value);
                    if (isSuccess)
                    {
                        result.Push(value);
                    }
                } while (isSuccess);
            }
            return result;
        }
    }
}
