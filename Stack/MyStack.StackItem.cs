﻿namespace Stack
{
    public partial class MyStack<T>
    {
        private class StackItem<V>
        {
            public V? Value { get; private set; }
            public StackItem<V>? PreviousItem { get; private set; }

            public StackItem(V? value, StackItem<V>? previousItem)
            {
                Value = value;
                PreviousItem = previousItem;
            }
        }
    }
}
