﻿/*
 * Домашнее задание

Реализация класс коллекции - Стэк
Цель: научится проектировать классы (обычные и статические), создавать в них методы и свойства

Нужно создать класс Stack у которого будут следующие свойства

    В нем будем хранить строки
    В качестве хранилища используйте список List
    Конструктор стека может принимать неограниченное количество входных параметров типа string, которые по порядку добавляются в стек

    Метод Add(string) - добавить элемент в стек
    Метод Pop() - извлекает верхний элемент и удаляет его из стека. При попытке вызова метода Pop у пустого стека - выбрасывать исключение с сообщением "Стек пустой"
    Свойство Size - количество элементов из Стека
    Свойство Top - значение верхнего элемента из стека. Если стек пустой - возвращать null

    Доп. задание 1
    Создайте класс расширения StackExtensions и добавьте в него метод расширения Merge, который на вход принимает стек s1, и стек s2.
    Все элементы из s2 должны добавится в s1 в обратном порядке
    Сам метод должен быть доступен в класс Stack

    Доп. задание 2
    В класс Stack и добавьте статический метод Concat, который на вход неограниченное количество параметров типа Stack
    и возвращает новый стек с элементами каждого стека в порядке параметров, но сами элементы записаны в обратном порядке

    Доп. задание 3
    Вместо коллекции - создать класс StackItem, который
    доступен только для класс Stack (отдельно объект класса StackItem вне Stack создать нельзя)
    хранит текущее значение элемента стека
    ссылку на предыдущий элемент в стеке
    Методы, описанные в основном задании переделаны под работу со StackItem
 */

namespace Stack
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // *** Проверка основных операций ***
            MyStack<string> stack = new("a", "b", "c");
            Console.WriteLine($"Создан стек со значениями 'a', 'b', 'c'. Count = {stack.Count}, Top element = '{stack.Peek()}'");
            string? deleted = stack.Pop();
            Console.WriteLine($"Извлек верхний элемент '{deleted}', Count = {stack.Count}");
            stack.Push("d");
            Console.WriteLine($"Добавил элемент. Count = {stack.Count}, Top element = '{stack.Peek()}'");
            stack.Pop();
            stack.Pop();
            stack.Pop();
            Console.WriteLine($"Извлек все элементы. Count = {stack.Count}");
            try
            {
                Console.WriteLine("Попытка считать элемент в пустом стеке...");
                stack.Peek();
            }
            catch (InvalidOperationException exc)
            {
                Console.WriteLine($"Возникло исключение: {exc.Message}");
            }
            try
            {
                Console.WriteLine("Попытка извлечь элемент из пустого стека...");
                stack.Pop();
            }
            catch (InvalidOperationException exc)
            {
                Console.WriteLine($"Возникло исключение: {exc.Message}");
            }

            // *** Проверка TryPop() ***
            Console.WriteLine("Попытка извлечения элемента из пустого стека через TryPop()...");
            bool isSuccess = stack.TryPop(out string? result);
            Console.WriteLine($"Успешность: {isSuccess}, значение: {result}");
            stack.Push("R");
            Console.WriteLine("Попытка извлечения элемента из не-пустого стека через TryPop()...");
            isSuccess = stack.TryPop(out result);
            Console.WriteLine($"Успешность: {isSuccess}, значение: {result}");
            stack.Clear();
            Console.WriteLine($"Стек очищен с помощью Clear(). Count = {stack.Count}");

            // *** Проверка Concat() ***
            MyStack<string> st1 = new("A", "B", "C");
            MyStack<string> st2 = new("1", "2", "3");
            MyStack<string> st3 = new("x", "y", "z");
            MyStack<string> stackConcat = MyStack<string>.Concat(st1, st2, st3);
            Console.WriteLine("Объединение трех стеков Concat(). Результат с вершины до корня:");
            do
            {
                isSuccess = stackConcat.TryPop(out string? value);
                if (isSuccess)
                {
                    Console.Write(value + ", ");
                }
            } while (isSuccess);
            Console.WriteLine();

            // *** Проверка Merge() ***
            // st2 будет опустошен
            st1.Merge(ref st2);
            Console.WriteLine("Объединение стеков с помощью Merge(). Результат с вершины до корня:");
            do
            {
                isSuccess = st1.TryPop(out string? value);
                if (isSuccess)
                {
                    Console.Write(value + ", ");
                }
            } while (isSuccess);
            Console.WriteLine();
        }
    }
}